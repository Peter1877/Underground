﻿using cadClientes.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadClientes.Control
{
    class ClienteDAO
    {
        public void criar(BeansCliente cli)
        {
            SqlCommand cmd = null;
            SqlConnection con = null;
            string connectionString = @"Server=(localdb)\MSSQLLocalDB;Database=dbcrud;Trusted_Connection=True;";
            String sql = "INSERT INTO CLIENTE(nome, sobrenome, cpf, rg, email, telefone) VALUES ('" + cli.getNome() + "','" + cli.getSobrenome() + "','" + cli.getCpf() + "','" + cli.getRg() + "','" + cli.getEmail() + "','" + cli.getTelefone() + "')";
            
            con = new SqlConnection(connectionString);
            cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            con.Open();
            try
            {
                int i = cmd.ExecuteNonQuery();
                if (i > 0)
                {
                    MessageBox.Show("Cadastro realizado com sucesso!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            finally
            {
                con.Close();
            }
        }
        public BeansCliente Buscar(BeansCliente cli)
        {
            BeansCliente cliente = new BeansCliente();
            SqlCommand cmd = null;
            SqlConnection con = null;
            string connectionString = @"Server=(localdb)\MSSQLLocalDB;Database=dbcrud;Trusted_Connection=True;";
            string sql = "SELECT * FROM CLIENTE WHERE id = '" + cli.getId() + "'";
            con = new SqlConnection(connectionString);
            cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;
            con.Open();
            try
            {
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    cliente.setId((int)reader[0]);
                    cliente.setNome(reader[1].ToString());
                    cliente.setSobrenome(reader[2].ToString());
                    cliente.setCpf(reader[3].ToString());
                    cliente.setRg(reader[4].ToString());
                    cliente.setEmail(reader[5].ToString());
                    cliente.setTelefone(reader[6].ToString());
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado com o Id informado!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            finally
            {
                con.Close();
            }
            return cliente;
        }
        public void deletar(BeansCliente cli)
        {
            SqlCommand cmd = null;
            SqlConnection con = null;
            string connectionString = @"Server=(localdb)\MSSQLLocalDB;Database=dbcrud;Trusted_Connection=True;";
            String sql = "DELETE FROM CLIENTE WHERE ID = '" + cli.getId().ToString() + "'";
            
            con = new SqlConnection(connectionString);
            cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            con.Open();
            try
            {
                int i = cmd.ExecuteNonQuery();
                if (i > 0)
                {
                    MessageBox.Show("Eliminado com sucesso");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            finally
            {
                con.Close();
            }
        }
        public void atualizar(BeansCliente cli)
        {
            SqlCommand cmd = null;
            SqlConnection con = null;
            string connectionString = @"Server=(localdb)\MSSQLLocalDB;Database=dbcrud;Trusted_Connection=True;";
            String sql = "UPDATE CLIENTE SET nome = '" + cli.getNome() + "', sobrenome = '" + cli.getSobrenome() + "', cpf = '" + cli.getCpf() + "', rg = '" + cli.getRg() + "', email = '" + cli.getEmail() + "', telefone = '" + cli.getTelefone() + "' WHERE ID = " + cli.getId();
            
            con = new SqlConnection(connectionString);
            cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            con.Open();
            try
            {
                int i = cmd.ExecuteNonQuery();
                if (i > 0)
                {
                    MessageBox.Show("Atualizado com sucesso");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.ToString());
            }
            finally
            {
                con.Close();
            }
        }
    }
}
