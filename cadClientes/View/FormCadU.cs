﻿using cadClientes.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadClientes.View
{
    public partial class FormCadU : Form
    {
        public FormCadU()
        {
            InitializeComponent();
        }

        private void btnCad_Click(object sender, EventArgs e)
        {
            String login = txtLogin.Text;
            String senha = txtSenha.Text;
            String funcao = txtFuncao.Text;

            BeansUsuario user = new BeansUsuario();
            user.setFuncao(funcao);
            user.setLogin(login);
            user.setSenha(senha);
            user.criar();
        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new Main();
            f.Closed += (s, args) => this.Close();
            f.Show();
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new frmLogin();
            f.Closed += (s, args) => this.Close();
            f.Show();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new Main();
            f.Closed += (s, args) => this.Close();
            f.Show();
        }
    }
}
