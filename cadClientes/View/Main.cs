﻿using cadClientes.Control;
using cadClientes.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cadClientes.View
{
    public partial class Main : Form
    {
        bool buscando = false;
        bool novo = false;
        bool editar = false;
        ClienteDAO controle = new ClienteDAO();
        BeansCliente cliente = new BeansCliente();
        BeansCliente re = new BeansCliente();
        public void AtivarTextos()
        {
            txtId.Enabled = true;
            txtCpf.Enabled = true;
            txtEmail.Enabled = true;
            txtNome.Enabled = true;
            txtRg.Enabled = true;
            txtSobre.Enabled = true;
            txtTel.Enabled = true;

        }
        public void DesativarTextos()
        {
            txtId.Enabled = false;
            txtCpf.Enabled = false;
            txtEmail.Enabled = false;
            txtNome.Enabled = false;
            txtRg.Enabled = false;
            txtSobre.Enabled = false;
            txtTel.Enabled = false;
        }
        public void AtivarBotoes()
        {
            btnNovo.Enabled = true;
            btnEditar.Enabled = true;
            btnIr.Enabled = true;
            btnBuscar.Enabled = true;
            btnCancelar.Enabled = true;
        }
        public void DesativarBotoes()
        {
            btnNovo.Enabled = false;
            btnEditar.Enabled = false;
            btnDel.Enabled = false;
            btnIr.Enabled = false;
            btnBuscar.Enabled = false;
            btnCancelar.Enabled = false;
        }
        public void limparCampos()
        {

            txtCpf.Text = "";
            txtEmail.Text = "";
            txtId.Text = "0";
            txtNome.Text = "";
            txtRg.Text = "";
            txtSobre.Text = "";
            txtTel.Text = "";

        }
        public Main()
        {
            InitializeComponent();
            DesativarTextos();
            btnDel.Enabled = false;
            btnIr.Enabled = false;
            btnEditar.Enabled = false;
            btnCancelar.Enabled = false;
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {


            buscando = true;
            DesativarTextos();
            txtId.Enabled = true;
            DesativarBotoes();
            btnIr.Enabled = true;
            btnCancelar.Enabled = true;

            
        }

        private void btnIr_Click(object sender, EventArgs e)
        {
            DesativarTextos();
            if (buscando)
            {
                re.setId((int)txtId.Value);
                re = controle.Buscar(re);
                txtCpf.Text = re.getCpf();
                txtEmail.Text = re.getEmail();
                txtId.Value = re.getId();
                txtNome.Text = re.getNome();
                txtRg.Text = re.getRg();
                txtSobre.Text = re.getSobrenome();
                txtTel.Text = re.getTelefone();

                AtivarBotoes();
                btnIr.Enabled = false;
                btnBuscar.Enabled = false;
                btnDel.Enabled = true;
                btnNovo.Enabled = false;
            }else if (novo)
            {
                
                re.setCpf(txtCpf.Text);
                re.setEmail(txtEmail.Text);
                re.setNome(txtNome.Text);
                re.setRg(txtRg.Text);
                re.setSobrenome(txtSobre.Text);
                re.setTelefone(txtTel.Text);
                re.criar();

                limparCampos();
                AtivarBotoes();
                btnIr.Enabled = false;
                btnEditar.Enabled = false;
                btnCancelar.Enabled = false;
            }else if (editar)
            {
                re.setId((int)txtId.Value);
                re.setCpf(txtCpf.Text);
                re.setEmail(txtEmail.Text);
                re.setNome(txtNome.Text);
                re.setRg(txtRg.Text);
                re.setSobrenome(txtSobre.Text);
                re.setTelefone(txtTel.Text);
                re.atualizar();

                limparCampos();
                AtivarBotoes();
                btnIr.Enabled = false;
                btnEditar.Enabled = false;
            }


            buscando = false;
            novo = false;
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            AtivarTextos();
            txtId.Enabled = false;
            novo = true;
            DesativarBotoes();
            btnCancelar.Enabled = true;
            btnIr.Enabled = true;
        }

        private void btnDel_Click(object sender, EventArgs e)
        {

            cliente.setId((int)txtId.Value);
            cliente.deletar();
            limparCampos();
            DesativarBotoes();
            btnNovo.Enabled = true;
            btnBuscar.Enabled = true;
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            DesativarBotoes();
            btnIr.Enabled = true;
            AtivarTextos();
            txtId.Enabled = false;
            editar = true;
            btnCancelar.Enabled = true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DesativarTextos();
            DesativarBotoes();
            btnBuscar.Enabled = true;
            btnNovo.Enabled = true;
            txtId.Enabled = false;
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
                this.Hide();
                Form f = new frmLogin();
                f.Closed += (s, args) => this.Close();
                f.Show();
        }
        

    private void usuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form f = new FormCadU();
            f.Closed += (s, args) => this.Close();
            f.Show();

        }
    }
}
